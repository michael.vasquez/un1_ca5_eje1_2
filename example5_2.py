# pedir una lista de numero
# mostrar en la pantalla el numero menor y el numero mayor
# autor = "Michael Vasquez"
# email = "michael.vasquez@unl.edu.ec

cont = 0
total = 0
lista = []

while True:

    valor = input("Introduce un numero (o 'fin' para terminar): ")
    lista.append(valor)
    if valor.lower() in "fin":
        break
    try:
        total += float (valor)
        cont  += 1
        mayor = max (lista)
        menor = min (lista)
    except ValueError:
        print("ERROR, no es un numero\nIntentalo de nuevo...")

print("El total es: ", total)
print("Haz introducido: ", cont, " numeros")
print("Lista de numero:", lista)
print("El numero mayor ingresado es: ", float(mayor))
print("El numero menor ingresado es: ", float(menor))
